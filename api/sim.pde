// press 'a' to increase reference, 's' to decrease, 
//and 'm' to enable mouse reference setting

// Moram imati klasu koja prestavlja paket upravljanja pod nazivom Actuation
class Actuation
{
  float ciljniUgao = 0;
}

// Moram imati klasu koja prestavlja merni rezultat pod nazivom Measurement
class Measurement
{
  float pozicija = 0;
}

// Moja simulacija mora da nasledi klasu Simulation i da implementira
// metode Update, Measure, Control, Visualize
float zpozicija = 1;
Boolean mouseEnabled = false;

class BallAndBeam extends Simulation
{
  float dt = 0;
  float g = 9.81;
  float pozicija = 0;
  float brzina = 0;
  float ubrzanje = 0;
  float ugao = 0;
  float pUgao = 0;
  float T = 0.5;
  float greska=0;
  float pgreska=0;
  float ppgreska=0;
  float kd=0.08;
  float kp=0.3*0.08;
  BallAndBeam ()
  {
    // Podesim frekvenciju update-jta simulacije da bude 10 puta veca od
    // frame rate-jta (30 fps)
    SetUpdateMul(10);

    // Podesim periodu kontrolera tako da bude 5 puta manja od perioda simulacije
    SetControlMul(1);

    dt = GetUpdatePeriod();
  }

  // Napravim korak simulacije. Npr. uradim numericku integraciju diferencijalne jednacine.
  // Dato mi je i upravljanje koje je izgenerisao kontroler.
  void Update(Actuation input)
  {
    ugao = (input.ciljniUgao*dt+T*pUgao)/(T+dt);
    pUgao = ugao;
    ubrzanje = g*sin(ugao);
    brzina += ubrzanje*dt;
    pozicija += brzina*dt;
  }

  // Napravim merenja (merenja se uvek desava pre izvrsavanja kontrolera)
  Measurement Measure()
  {
    Measurement m = new Measurement();
    m.pozicija = pozicija;
    return m;
  }



  // Izracunam novo upravljanje na osnovu merenja
  Actuation Control(Measurement m)
  {
    Actuation o = new Actuation();
    greska=zpozicija-pozicija;
    println("Greska: "+greska+" zeljena pozicija "+zpozicija+" pozicija "+pozicija);

    o.ciljniUgao = kp*greska+kd*(greska-pgreska)/dt;
    ppgreska = pgreska;
    pgreska=greska;

    if (o.ciljniUgao>PI/2) o.ciljniUgao=PI/2;
    else if (o.ciljniUgao<-PI/2) o.ciljniUgao=-PI/2;

    println("Ugao na izlazu kontrole: "+o.ciljniUgao);
    return o;
  }

  // Vizualizujem rezultat simulacije (frame rate je 30 fps)
  void Visualize()
  {
    background(255);
    stroke(255, 0, 0);
    strokeCap(PROJECT);
    strokeWeight(10);
    line(320-250*cos(ugao), 195+250*sin(ugao), 320+250*cos(ugao), 195-250*sin(ugao));
    strokeWeight(1);
    fill(0, 255, 0);
    stroke(0, 0, 255);
    ellipseMode(RADIUS);
    ellipse(320-pozicija*100*cos(ugao), 175+pozicija*100*sin(ugao), 10, 10);

    triangle(320, 200, 360, 250, 280, 250);
    if (mouseEnabled) zpozicija=(-mouseX+320.0)/100.0;
  }
}

// Moram implementirati funkciju koja inicijalizuje simulaciju
// i u njoj postaviti globalnu promenljivu sim na instancu moje simulacije
void InitSimulation()
{
  surface.setSize(640, 360);
  stroke(255);

  sim = new BallAndBeam();
}

void keyPressed() {
  if (key == 'a') zpozicija+=0.1;
  else if (key == 's') zpozicija-=0.1;
  else if (key == 'm') mouseEnabled = !mouseEnabled;
  println(zpozicija);
}
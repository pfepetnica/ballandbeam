

static Simulation sim = null;

abstract class Simulation 
{ 
  abstract void Update(Actuation input);
  abstract Measurement Measure();
  abstract Actuation Control(Measurement m);
  abstract void Visualize();

  float GetUpdatePeriod()
  {
    return 1.0 / fps / updateMul;
  }

  float GetControlPeriod()
  {
    return GetUpdatePeriod() * measureMul;
  }

  void SetUpdateMul(int n)
  {
    updateMul = n;
  }

  void SetControlMul(int n)
  {
    measureMul = n;
  }

  // update period is 1.0 / fps / updateMul
  private int updateMul = 10;

  // Measurement period is update period * measureMul
  private int measureMul = 5;

  private Actuation currentOutput = new Actuation();

  private int fps = 30;

  private int measureCnt = 0;
} 

void setup() 
{
  InitSimulation();

   frameRate(sim.fps);  
}

void draw() 
{   
  for (int i = 0; i < sim.updateMul; i++)
  {
    sim.Update(sim.currentOutput);

    if (sim.measureCnt % sim.measureMul == 0)
    {
      sim.currentOutput = sim.Control(sim.Measure());
      sim.measureCnt = 0;
    }

    sim.measureCnt++;
  }

  sim.Visualize();
} 